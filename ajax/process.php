<?php
  function send_email($email, $message_body, $subject = "") {
    $host = $_SERVER["HTTP_HOST"];
    $host_no_www = str_replace("www.", "", $host);
    $from = "{$site_title} <noreply@{$host_no_www}>";

    if (empty($subject))
      $subject = "Сообщение с сайта {$host}";

    $headers  = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=utf-8\r\n";
    $headers .= "From: {$from}\r\n";

    $message = "
      <html>
        <head></head>
        <body style=\"font: normal 12px/14px Tahoma; color: #444444;\">
          <h3>Данные о клиенте:</h3>
          <p>{$message_body}</p>
          <p style=\"font-style: italic; color: #aaaaaa;\">Сообщение отправлено роботом и не требует ответа</p>
          <p>
            ____________________
            <br/>Это сообщение отправлено с сайта <a href=\"http://{$host}\">{$host}</a>
          </p>
        </body>
      </html>";

    if (!empty($email) && !empty($message_body))
      return mail($email, $subject, $message, $headers);
    else
      return false;
  }

  /*  Process form fields
  /*
  /*  $fields = array(
  /*    "field_name" => array(
  /*      "label" => "field_label",
  /*      "value" => "field_value",
  /*      "params" => "required|email|nl2br|strip_tags"
  /*    ),
  /*    ...
  /*  );
  */
  function process_fields($fields) {
    foreach ($fields as $key => $field) {
      $params = explode("|", str_replace(" ", "", $field["params"]));

      $label = trim($field["label"]);
      $value = trim($field["value"]);

      if (!empty($params)) {
        if (in_array("required", $params) && empty($value)) {
          $fields[$key]["error"] = "поле \"{$label}\" обязательно для заполнения";
          continue;
        }
        if (in_array("email", $params) && !filter_var($value, FILTER_VALIDATE_EMAIL)) {
          $fields[$key]["error"] = "поле \"{$label}\" должно содержать Ваш реальный E-mail адрес";
          continue;
        }
        if (in_array("strip_tags", $params)) {
          $value = strip_tags($value);
        }
        if (in_array("nl2br", $params)) {
          $value = nl2br($value);
        }

        $fields[$key]["value"] = $value;
      }
    }

    return $fields;
  }

  function process_errors($fields) {
    $errors = array();
    foreach ($fields as $field) {
      if (!empty($field["error"]))
        $errors[] = $field["error"];
    }

    return $errors;
  }

  function is_ajax() {
    return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
  }


