<?php
  error_reporting(0);
  include("process.php");

  define("DEFAULT_ERROR_MESSAGE", "ошибка отправки сообщения");
  define("DEFAULT_SUCCESS_MESSAGE", "Спасибо за обращение!\r\nМы свяжемся с Вами в ближайшее время.");

  function process_feedback_form() {
    $data = $_POST["data"];
    $time = date("d.m.Y H:i:s", time());
    $message_body = "";

    foreach ( $data as $name => $value ) {
      if(!empty($value)) {
        $name = str_replace('"', '', $name);
        $name = str_replace('\'', '', $name);
        $name = str_replace('\\', '', $name);

        $value = htmlspecialchars($value);

        if($name=="Фотография"){
          $message_body .= "<strong>${name}:</strong> <a href='http://".$_SERVER["HTTP_HOST"]."/uploadfiles/{$value}' target='_blank'>{$value}</a><br>";
        } else {
          $message_body .= "<strong>${name}:</strong> {$value}<br>";
        }
      }
    }
    $message_body .= "<strong>Время отправки:</strong> {$time}<br>";

    $send = send_email('zakaz@zavisaet.ru', $message_body, $subject);

    if ($send)
      return json_encode(array("success" => DEFAULT_SUCCESS_MESSAGE, "errors" => array()));
    else
      return json_encode(array("success" => "", "errors" => array(DEFAULT_ERROR_MESSAGE)));
  }


  // Обработка загрузки фотографии
  if($_GET['uploadfile']){
    require(dirname(__FILE__) . '/Uploader.php');
    // Directory where we're storing uploaded images
    // Remember to set correct permissions or it won't work
    $upload_dir = '../uploadfiles/';
    $valid_extensions = array('gif', 'png', 'jpeg', 'jpg');
    $uploader = new FileUpload('uploadfile');
    $ext = $uploader->getExtension(); // Get the extension of the uploaded file

    $filename = time().'.'.$ext;
    while(file_exists($upload_dir.$filename)){
      $filename = time().'.'.$ext;
    }

    $uploader->newFileName = $filename;
    // Handle the upload
    $result = $uploader->handleUpload($upload_dir, $valid_extensions);
    if (!$result) {
      exit(json_encode(array('success' => false, 'msg' => $uploader->getErrorMsg())));
    }
    echo json_encode(array('success' => true, 'filename' => $filename));
    exit;
  }


  if (!is_ajax()) return;

  $security = $_POST["security"];
  if (empty($security)) return;

  $result = process_feedback_form();
  echo $result;
